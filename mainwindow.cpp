#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "attitude_indicator.h"

#include <qwt_compass.h>
#include <qserialportinfo.h>
#include <qdebug.h>
#include <qmessagebox.h>
#include <qapplication.h>
#include <qtimer.h>
#include <qtextstream.h>
#include <qbytearray.h>
#include <qwt_dial_needle.h>
#include <QPalette>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    port(parent),
    processingTrigger(parent)
{
    QColor base(QColor(Qt::darkGray).dark(150));
    QPalette palette;
    palette.setColor( QPalette::Base, base );
    palette.setColor( QPalette::Window, base.dark( 150 ) );
    palette.setColor( QPalette::Mid, base.dark( 110 ) );
    palette.setColor( QPalette::Light, base.light( 170 ) );
    palette.setColor( QPalette::Dark, base.dark( 170 ) );
    palette.setColor( QPalette::Text, base.dark( 200 ).light( 800 ) );
    palette.setColor( QPalette::WindowText, base.dark( 200 ) );
    this->setPalette(palette);
    d_ai = new AttitudeIndicator( this );
    d_ai->scaleDraw()->setPenWidth( 3 );

    ui->setupUi(this);
    speed = new SpeedoMeter();
    currentSpeed = 0;
    speed->setValue(currentSpeed);
    //setPalette(colorTheme(QColor(Qt::darkGray).dark(150)));
    direction = new QwtCompass();
    direction->setNeedle( new QwtDialSimpleNeedle( QwtDialSimpleNeedle::Ray,
                                                   false, Qt::yellow ) );
    direction->setFrameShadow(QwtDial::Plain);
    direction->setValue(90);

    ui->boardGadget->addWidget(speed);
    ui->boardGadget->addSpacerItem(new QSpacerItem(20, 20));
    ui->boardGadget->addWidget(direction);
    ui->boardGadget->addWidget(d_ai);
    // Initialize port related objects
    portInitialized = false;
    currentPortCommand = NoCommand;
    commandBytesProcessed = 0;
    setupComPort();
    // init commandLengths
    setupCommandLengths();

    connect(&port, SIGNAL(readyRead()), this, SLOT(readData()));
    connect(&processingTrigger, SIGNAL(timeout()), this,
            SLOT(processPortCommands()));
    processingTrigger.start();
    if (!portInitialized) {
        QMessageBox::critical(parent, "Error!", "Port not found, will exit.");
        //QTimer::singleShot(1, qApp, SLOT(quit()));
        return;
    }

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setupComPort()
{
    QList<QSerialPortInfo> portsList = QSerialPortInfo::availablePorts();
    QList<QSerialPortInfo>::const_iterator it = portsList.cbegin();
    while (it != portsList.cend()) {
        qDebug() << it->portName() << it->manufacturer() << it->description();
        if (it->manufacturer().compare("mbed", Qt::CaseInsensitive) == 0)
            break;
        it++;
    }
    if (it != portsList.cend()) {
        portInitialized = true;
        port.setPort(*it);
        if (!port.open(QIODevice::ReadWrite))
            qDebug() << "Failed to open port!";
        port.setBaudRate(QSerialPort::Baud9600);
        port.setDataBits(QSerialPort::Data8);
        port.setParity(QSerialPort::NoParity);
        port.setStopBits(QSerialPort::OneStop);
    }
}

void MainWindow::setupCommandLengths()
{
    commandLengths[NoCommand] = 1;
    commandLengths[ModeSet] = 2;
    commandLengths[SpeedUpdate] = 2;
    commandLengths[SpeedSet] = 2;
    commandLengths[DirectionUpdate] = 3;
    commandLengths[DirectionSet] = 3;
    commandLengths[FirstDistanceSensorUpdate] = 2;
    commandLengths[SecondDistanceSensorUpdate] = 2;
    commandLengths[ThirdDistanceSensorUpdate] = 2;
    commandLengths[IrDistanceSensorUpdate] = 2;
    commandLengths[MetalSensorUpdate] = 2;
    commandLengths[AccelSensorUpdate] = 3;
    commandLengths[BatterySensorUpdate] = 2;

}

void MainWindow::readData()
{
    dataBuffer += port.readAll();
}

void MainWindow::processPortCommands()
{
    if (dataBuffer.size()) {
        if (currentPortCommand == NoCommand) {
            currentPortCommand = (PortCommand)(int)dataBuffer[0];
            commandBytesProcessed = 1;
            dataBuffer.remove(0, 1);
        } else {
            switch (currentPortCommand) {
            case SpeedUpdate:
                readSpeed();
                if (commandBytesProcessed == commandLengths[currentPortCommand])
                    speed->setValue(currentSpeed);
                break;
            case DirectionUpdate:

                readDirection();
                if (commandBytesProcessed == commandLengths[currentPortCommand]){
                    newDirection = 0;
                    direction ->setValue(currentDirection);
                }

                break;
            case FirstDistanceSensorUpdate:

                readFirstDistanceSensor();
                if (commandBytesProcessed == commandLengths[currentPortCommand]){

                    ui->firstDistanceSensorText->setText(QString::number(currentFirstSensorDistance));
                }
                break;
            case SecondDistanceSensorUpdate:

                 readSecondDistanceSensor();
                if (commandBytesProcessed == commandLengths[currentPortCommand]){

                    ui->secondDistanceSensorText->setText(QString::number(currentFirstSensorDistance));
                }

                break;
            case ThirdDistanceSensorUpdate:

                readThirdDistanceSensor();
                if (commandBytesProcessed == commandLengths[currentPortCommand]){

                    ui->thirdDistanceSensorText->setText(QString::number(currentFirstSensorDistance));
                }

                break;
            case IrDistanceSensorUpdate:
                readIrDistanceSensor();
                if (commandBytesProcessed == commandLengths[currentPortCommand]){
                    ui->irSensorText->setText(QString::number(currentIrDistance));
                    //reset if ir > nush cat :P
                }
                break;
            case MetalSensorUpdate:

                 readMetalSensor();
                if (commandBytesProcessed == commandLengths[currentPortCommand]){
                    if(newCurrentMetalSensorValue == 1){
                        currentMetalSensorValue = true;
                    }else{
                        currentMetalSensorValue = false;
                    }
                    ui->metalSensorCheckBox->setChecked(currentMetalSensorValue);
                }

                break;
            case AccelSensorUpdate:
                readAccelSensor();
                  if (commandBytesProcessed == commandLengths[currentPortCommand] - 1){
                      d_ai->setAngle(currentAccelY > 0 ? currentAccelY : currentAccelY + 360);
                      qDebug() << currentAccelY;
                  }
                  if (commandBytesProcessed == commandLengths[currentPortCommand]){
                      d_ai->setGradient(-currentAccelX / 90.0);
                  }
                break;
            case BatterySensorUpdate:
                readBatterySensor();
                if (commandBytesProcessed == commandLengths[currentPortCommand]){
                    ui->batteryProgressBar->setValue(currentBatteryLevel);
                }
                break;
            default: //Invalid command, reset
                dataBuffer.clear();
                currentPortCommand = NoCommand;
                commandBytesProcessed = 0;
                newDirection = 0;
            }
        }
    }
    if (commandBytesProcessed == commandLengths[currentPortCommand]) {
        commandBytesProcessed = 0;
        currentPortCommand = NoCommand;
    }

}
void MainWindow::resetAll(){
    currentSpeed = 0;
    currentDirection = 0;
    newDirection = 0;
    currentFirstSensorDistance = 0;
    currentSecondSensorDistance = 0;
    currentThirdSensorDistance = 0;
    currentIrDistance = 0;
    currentMetalSensorValue = false;
    newCurrentMetalSensorValue = 0;
    currentAccelX = 0;
    currentAccelY = 0;
    currentBatteryLevel = 0;
}

void MainWindow::readSpeed()
{
    currentSpeed = dataBuffer[0];
    dataBuffer.remove(0, 1);
    commandBytesProcessed++;
}
void MainWindow::readDirection(){

    newDirection += dataBuffer[0] << (commandBytesProcessed - 1);
    dataBuffer.remove(0,1);
    commandBytesProcessed ++;

}
void MainWindow::readFirstDistanceSensor(){

    currentFirstSensorDistance = dataBuffer[0];
    dataBuffer.remove(0,1);
    commandBytesProcessed ++;
}
void MainWindow::readSecondDistanceSensor(){

    currentSecondSensorDistance = dataBuffer[0];
    dataBuffer.remove(0,1);
    commandBytesProcessed ++;
}
void MainWindow::readThirdDistanceSensor(){

    currentThirdSensorDistance = dataBuffer[0];
    dataBuffer.remove(0,1);
    commandBytesProcessed ++;
}
void MainWindow:: readIrDistanceSensor(){
    currentIrDistance = dataBuffer[0];
    dataBuffer.remove(0,1);
    commandBytesProcessed ++;
}
void MainWindow::readMetalSensor(){
     newCurrentMetalSensorValue = dataBuffer[0];
     dataBuffer.remove(0,1);
     commandBytesProcessed ++;
}

void MainWindow::readAccelSensor(){
    quint8 aux = dataBuffer[0];
    if(commandBytesProcessed == 1){
        currentAccelX = aux;
    }
     if(commandBytesProcessed == 2){
         currentAccelY = aux;
     }
     dataBuffer.remove(0,1);
     commandBytesProcessed ++;
}
void MainWindow::readBatterySensor(){
     currentBatteryLevel = dataBuffer[0];
     dataBuffer.remove(0,1);
     commandBytesProcessed ++;
}
//void MainWindow::changeAngle()
//{
//    static double offset = 0.05;

//    double angle = d_ai->angle();
//    if ( angle > 180.0 )
//        angle -= 360.0;

//    if ( ( angle < -90.0 && offset < 0.0 ) ||
//        ( angle > 90.0 && offset > 0.0 ) )
//    {
//        offset = -offset;
//    }

//    d_ai->setAngle( angle + offset );
//}

//void MainWindow::changeGradient()
//{
//    static double offset = 0.005;

//    double gradient = d_ai->gradient();

//    if ( ( gradient < -0.05 && offset < 0.0 ) ||
//        ( gradient > 0.05 && offset > 0.0 ) )
//    {
//        offset = -offset;
//    }

//    d_ai->setGradient( gradient + offset );
//}
