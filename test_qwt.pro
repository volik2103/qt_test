#-------------------------------------------------
#
# Project created by QtCreator 2014-04-17T16:27:54
#
#-------------------------------------------------

QT       += core gui serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = test_qwt
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    speedo_meter.cpp \
    attitude_indicator.cpp

HEADERS  += mainwindow.h \
    speedo_meter.h \
    attitude_indicator.h

FORMS    += mainwindow.ui

win32:CONFIG(release, debug|release): LIBS += -LD:/Qwt-6.1.0/lib/ -lqwt
else:win32:CONFIG(debug, debug|release): LIBS += -LD:/Qwt-6.1.0/lib/ -lqwtd
else:unix: LIBS += -L/usr/local/qwt-6.1.0/lib/ -lqwt

win32: {
  INCLUDEPATH += D:/Qwt-6.1.0/include
  DEPENDPATH += D:/Qwt-6.1.0/include
} else:
unix: {
  INCLUDEPATH += /usr/local/qwt-6.1.0/include
  DEPENDPATH += /usr/local/qwt-6.1.0/include
}
