#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSerialPort>
#include <QBuffer>
#include <QTimer>
#include <QMap>
#include <qpalette.h>

#include "speedo_meter.h"
#include <qwt_compass.h>

namespace Ui {
class MainWindow;
}
class QwtDial;
class AttitudeIndicator;
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void readData();
    void processPortCommands();

private:
    enum PortCommand {
        NoCommand,
        ModeSet,
        SpeedUpdate,
        SpeedSet,
        DirectionUpdate,
        DirectionSet,
        FirstDistanceSensorUpdate,
        SecondDistanceSensorUpdate,
        ThirdDistanceSensorUpdate,
        IrDistanceSensorUpdate,
        MetalSensorUpdate,
        AccelSensorUpdate,
        BatterySensorUpdate
    };

    QPalette colorTheme( const QColor & ) const;
    QwtDial *createDial( int pos );

    AttitudeIndicator *d_ai;
    Ui::MainWindow *ui;
    SpeedoMeter *speed;
    quint8 currentSpeed;
    quint16 currentDirection;
    quint16 newDirection;
    quint8 currentFirstSensorDistance;
    quint8 currentSecondSensorDistance;
    quint8 currentThirdSensorDistance;
    quint8 currentIrDistance;
    bool currentMetalSensorValue;
    quint8 newCurrentMetalSensorValue;
    qint8 currentAccelX;
    qint8 currentAccelY;
    quint8 currentBatteryLevel;
    QwtCompass *direction;
    void setupComPort();
    void setupCommandLengths();
    void readSpeed();
    void readDirection();//2oct
    void readFirstDistanceSensor();
    void readSecondDistanceSensor();
    void readThirdDistanceSensor();
    void readIrDistanceSensor();
    void readMetalSensor();
    void readAccelSensor();
    void readBatterySensor();
    void resetAll();
    QSerialPort port;
    QByteArray dataBuffer;
    QTimer processingTrigger;
    bool portInitialized;
    PortCommand currentPortCommand;
    QMap<int, int> commandLengths;
    int commandBytesProcessed;
};

#endif // MAINWINDOW_H
